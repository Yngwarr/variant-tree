CC=g++ -std=c++17
BIN=tree
TEST_BIN=test_bin

all:
	$(CC) src/*.cc -o $(BIN)
test:
	$(CC) -lcpptest src/node.cc src/box.cc tests/main.cc -o $(TEST_BIN)
	./$(TEST_BIN)
lint:
	python2 cpplint.py src/* tests/*
clean:
	rm $(BIN) $(TEST_BIN)
