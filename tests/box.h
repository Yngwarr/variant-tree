/* Copyright 2020 Igor Kislitsyn */
#ifndef _2GIS_TEST_TEST_BOX_H_
#define _2GIS_TEST_TEST_BOX_H_

#include <cpptest.h>
#include <vector>
#include <regex>
#include <memory>
#include <string>

#include "../src/box.h"

class BoxTestSuite : public Test::Suite {
    public:
        BoxTestSuite() {
            TEST_ADD(BoxTestSuite::factory_test);
            TEST_ADD(BoxTestSuite::reg_test);
        }

    private:
        void factory_test() {
            std::unique_ptr<Box> a(make_box(5));
            std::unique_ptr<Box> b(make_box(3.14));
            std::unique_ptr<Box> c(make_box("glass"));
            std::unique_ptr<Box> d(parse_box("5"));
            std::unique_ptr<Box> e(parse_box("3.14"));
            std::unique_ptr<Box> f(parse_box("\"glass\""));

            TEST_ASSERT(a->str() == "5");
            TEST_ASSERT(b->str() == "3.14");
            TEST_ASSERT(c->str() == "\"glass\"");
            TEST_ASSERT(d->str() == "5");
            TEST_ASSERT(e->str() == "3.14");
            TEST_ASSERT(f->str() == "\"glass\"");
        }

        void reg_test() {
            std::vector<std::string> is = {"123", "+312", "-136"};
            std::vector<std::string> fs = {"-1.2", "1.2", "1e12",
                "1.34e-2", "1E2"};
            std::smatch m;

            for (int i = 0; i < is.size(); ++i) {
                TEST_ASSERT(reg::is_int(is[i]));
                TEST_ASSERT(!reg::is_real(is[i]));
            }

            for (int i = 0; i < fs.size(); ++i) {
                TEST_ASSERT(reg::is_real(fs[i]));
                TEST_ASSERT(!reg::is_int(fs[i]));
            }
        }
};

#endif  // _2GIS_TEST_TEST_BOX_H_
