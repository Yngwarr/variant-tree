/* Copyright 2020 Igor Kislitsyn */
#ifndef _2GIS_TEST_TEST_TREE_H_
#define _2GIS_TEST_TEST_TREE_H_

#include <cpptest.h>
#include <sstream>
#include <string>

#include "../src/node.h"
#include "../src/box.h"

class TreeTestSuite : public Test::Suite {
    public:
        TreeTestSuite() {
            TEST_ADD(TreeTestSuite::write_test);
            TEST_ADD(TreeTestSuite::read_test);
        }

    private:
        const std::string TREE = "8 (3.14 (7 \"plus\" (\"plus\")) 13 (\"see\"))";
        void write_test() {
            Node root(make_box(8));
            root
                .branch(make_box(3.14))
                ->branch(make_box(7));
            root
                .branch(make_box(13))
                ->branch(make_box("see"));
            root.child(0)
                ->branch(make_box("plus"))
                ->branch(make_box("plus"));

            std::stringstream ss;

            ss << root;
            TEST_ASSERT(ss.str() == TREE);
        }

        void read_test() {
            std::stringstream is(TREE);
            std::stringstream os;
            Node root;
            is >> root;
            os << root;

            TEST_ASSERT(os.str() == TREE);
        }
};

#endif  // _2GIS_TEST_TEST_TREE_H_
