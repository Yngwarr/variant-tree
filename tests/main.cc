/* Copyright 2020 Igor Kislitsyn */
#include <cpptest.h>
#include <iostream>

#include "box.h"
#include "node.h"

int main(int argc, char **argv) {
    Test::TextOutput output(Test::TextOutput::Verbose);
    BoxTestSuite bs;
    TreeTestSuite ts;
    return (bs.run(output) && ts.run(output)) ? EXIT_SUCCESS : EXIT_FAILURE;
}
