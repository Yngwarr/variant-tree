/* Copyright 2020 Igor Kislitsyn */
#include "node.h"

#include <string>
#include <exception>
#include <memory>
#include <utility>

Node::Node() {}

Node::Node(Box *n) {
    value(n);
}

const std::unique_ptr<Node>& Node::branch(Box *n) {
    children.push_back(std::unique_ptr<Node>(new Node(n)));
    return children.back();
}

const std::unique_ptr<Node>& Node::child(std::size_t n) {
    return children[n];
}

void Node::value(Box *val) {
    _value = std::shared_ptr<Box>(val);
}

std::shared_ptr<Box> Node::value() {
    return _value;
}

void Node::print() {
    std::cout << value()->str() << std::endl;
    for (int i = 0; i < children.size(); ++i) {
        children[i]->print("", i == children.size() - 1);
    }
}

void Node::print(std::string prefix, bool last) {
    std::cout
        << prefix
        << (last ? "└── " : "├── ")
        << value()->str() << std::endl;
    for (int i = 0; i < children.size(); ++i) {
        children[i]->print(prefix + (last ? "    " : "│   "),
            i == children.size() - 1);
    }
}

std::ostream& operator<<(std::ostream& os, const Node& node) {
    std::size_t size = node.children.size();
    os << (*node._value);
    if (size > 0) {
        os << " (";
        for (std::size_t i = 0; i < size; ++i) {
            os << (*node.children[i]);
            if (i != size - 1) {
                os << ' ';
            }
        }
        os << ')';
    }
    return os;
}

namespace lex {
    inline bool breaks_token(char ch) {
        return ch == '('
            || ch == ')'
            || ch == EOF
            || ch == ' '
            || ch == '\t'
            || ch == '\n';
    }

    std::string read_token(std::istream& is) {
        std::string token;
        char ch;

        is >> std::noskipws;

        while (!is.eof()) {
            ch = is.peek();
            if (breaks_token(ch)) {
                break;
            }
            is >> ch;
            token.push_back(ch);
        }

        is >> std::skipws;
        return token;
    }
}

std::istream& operator>>(std::istream& is, Node& node) {
    char ch;
    std::vector<std::unique_ptr<Node>> children;

    std::string token = lex::read_token(is);
    auto value = std::shared_ptr<Box>(parse_box(token));

    if (is.peek() == ' ') {
        is >> std::ws;
    }
    if (is.peek() == '(') {
        is >> ch;
        while (is.peek() != ')') {
            auto n = std::unique_ptr<Node>(new Node());
            is >> (*n) >> std::ws;
            children.push_back(std::move(n));
        }
        is >> ch;
    }

    node._value = value;
    node.children = std::move(children);

    return is;
}
