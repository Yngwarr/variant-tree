/* Copyright 2020 Igor Kislitsyn */
#include "box.h"

namespace reg {
    bool is_int(std::string str) {
        std::regex integer("[+-]?\\d+");
        std::smatch m;
        return std::regex_match(str, m, integer);
    }

    bool is_real(std::string str) {
        std::regex real("[+-]?\\d+\\.\\d+");
        std::regex scientific("[+-]?\\d+(\\.\\d+)?[Ee][+-]?\\d+");
        std::smatch m;
        return std::regex_match(str, m, real)
            || std::regex_match(str, m, scientific);
    }
};

template<>
std::string BoxImpl<std::string>::str() const {
    std::stringstream ss;
    ss << '"' << value << '"';
    return ss.str();
}

Box* make_box(int n) {
    return new BoxImpl<int>(n);
}

Box* make_box(double n) {
    return new BoxImpl<double>(n);
}

Box* make_box(std::string n) {
    return new BoxImpl<std::string>(n);
}

Box* parse_box(std::string repr) {
    if (repr.front() == '"' && repr.back() == '"') {
        return make_box(repr.substr(1, repr.size() - 2));
    }
    if (reg::is_int(repr)) {
        return make_box(std::atoi(repr.c_str()));
    }
    if (reg::is_real(repr)) {
        return make_box(std::atof(repr.c_str()));
    }
    throw std::runtime_error("unknown box format: \'" + repr + "\'");
}

std::ostream& operator<<(std::ostream& os, const Box& box) {
    os << box.str();
    return os;
}
