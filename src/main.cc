/* Copyright 2020 Igor Kislitsyn */
#include <iostream>
#include <fstream>
#include <memory>
#include <cstring>
#include <cstdlib>

#include "node.h"
#include "box.h"

void print_help() {
    std::cerr << "Usage: ./tree [options]\n\n"
    "Options:\n"
    "  -i filename\tread a serialized tree from a specified file; reads from stdin by default\n"
    "  -o filename\twrite a serialized tree to a specified file; writes to stdout by default\n"
    "  -q, --quiet\tdon't print a tree in human-readable format\n"
    "  -h, --help\tprint this message\n\n"
    "Reads a tree written in a specific format, prints it to stdout in human-readable format,\n"
    "and writes it to a file.\n\n"
    "Tree format can be described in the following BNF:\n"
    "  <nodes>\t::= <node> | <node> <nodes>\n"
    "  <node>\t::= <value> | <value> (<nodes>)\n"
    "  <value>\t::= <integer> | <real> | <string>\n"
    "  <integer>\t-- an integer number, positive or negative\n"
    "  <real>\t-- a floating-point number, in either decimal or exponential notation\n"
    "  <string>\t-- an arbitrary string of text surrounded by double quotes\n\n"
    "For example:\n"
    "  8 (3.14 \"see\" (\"plus\" \"plus\") -12e3)\n\n"
    "Made by Igor Kislitsyn <k.yngvarr<at>gmail.com> using open-source technologies.\n";
}

int main(int argc, char **argv) {
    Node root;
    bool quiet = false;
    std::ifstream in_file;
    std::ofstream out_file;

    auto cleanup = [&] {
        if (in_file.is_open()) in_file.close();
        if (out_file.is_open()) out_file.close();
    };

    for (int i = 1; i < argc; ++i) {
        if (!std::strcmp("-h", argv[i]) || !std::strcmp("--help", argv[i])) {
            print_help();
            cleanup();
            return EXIT_SUCCESS;
        }
        if (!std::strcmp("-q", argv[i]) || !std::strcmp("--quiet", argv[i])) {
            quiet = true;
        } else if (!std::strcmp("-i", argv[i])) {
            if (i == argc - 1) {
                std::cerr << "error: -i must be followed by an argument\n";
                cleanup();
                return EXIT_FAILURE;
            }
            in_file.open(argv[++i]);
            if (in_file.fail()) {
                std::cerr << "error: can't open file \""
                    << argv[i]
                    << "\", sticking with stdin\n";
            }
        } else if (!std::strcmp("-o", argv[i])) {
            if (i == argc - 1) {
                std::cerr << "error: -o must be followed by an argument\n";
                cleanup();
                return EXIT_FAILURE;
            }
            out_file.open(argv[++i]);
            if (out_file.fail()) {
                std::cerr << "error: can't open file \""
                    << argv[i]
                    << "\", sticking with stdout\n";
            }
        } else {
            std::cerr << "error: unknown option \"" << argv[i] << "\"\n";
            std::cerr << "use -h or --help to see usage info\n";
            cleanup();
            return EXIT_FAILURE;
        }
    }

    try {
        (in_file.is_open() ? in_file : std::cin) >> root;
    } catch (std::runtime_error e) {
        std::cerr << "error: " << e.what() << std::endl;
        cleanup();
        return EXIT_FAILURE;
    }
    if (!quiet) root.print();
    (out_file.is_open() ? out_file : std::cout) << root << std::endl;

    cleanup();
    return EXIT_SUCCESS;
}
