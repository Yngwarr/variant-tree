/* Copyright 2020 Igor Kislitsyn */
#ifndef _2GIS_TEST_NODE_H_
#define _2GIS_TEST_NODE_H_

#include <iostream>
#include <vector>
#include <string>
#include <memory>

#include "box.h"

class Node {
    public:
        Node();
        explicit Node(Box *n);
        const std::unique_ptr<Node>& branch(Box *n);
        const std::unique_ptr<Node>& child(std::size_t n);
        void value(Box *val);
        std::shared_ptr<Box> value();
        void print();
        void print(std::string prefix, bool last);
        friend std::ostream& operator<<(std::ostream& os, const Node& node);
        friend std::istream& operator>>(std::istream& is, Node& node);

    private:
        std::shared_ptr<Box> _value;
        std::vector<std::unique_ptr<Node>> children;
};

#endif  // _2GIS_TEST_NODE_H_
