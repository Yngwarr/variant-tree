/* Copyright 2020 Igor Kislitsyn */
#ifndef _2GIS_TEST_BOX_H_
#define _2GIS_TEST_BOX_H_

#include <string>
#include <sstream>
#include <regex>
#include <exception>

namespace reg {
    bool is_int(std::string str);
    bool is_real(std::string str);
};

class Box {
    public:
        virtual ~Box() { }
        virtual std::string str() const = 0;
        friend std::ostream& operator<<(std::ostream& os, const Box& box);
};

std::ostream& operator<<(std::ostream& os, const Box& box);

template<typename T>
class BoxImpl : public Box {
    public:
        explicit BoxImpl(T val) {
            value = val;
        }
        std::string str() const {
            std::stringstream ss;
            ss << value;
            return ss.str();
        }

    private:
        T value;
};

template<>
std::string BoxImpl<std::string>::str() const;

Box* make_box(int n);
Box* make_box(double n);
Box* make_box(std::string n);
Box* parse_box(std::string repr);

#endif  // _2GIS_TEST_BOX_H_
